package com.xlongwei.light4j.extend;

import com.networknt.handler.Handler;
import com.networknt.handler.MiddlewareHandler;

import io.undertow.Handlers;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * 实现了next相关的重复逻辑，子类只需自定义handleRequest即可，还可根据需要覆盖isEnabled和register方法
 */
public class MyMiddlewareHandler implements MiddlewareHandler {
	protected volatile HttpHandler next;

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		Handler.next(exchange, next);
	}

	@Override
	public HttpHandler getNext() {
		return next;
	}

	@Override
	public MiddlewareHandler setNext(HttpHandler next) {
		Handlers.handlerNotNull(next);
		this.next = next;
		return this;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void register() {
	}

}
